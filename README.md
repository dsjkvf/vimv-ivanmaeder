vimv
====


## About

This is a fork of [`vimv.py`](https://github.com/ivanmaeder/vimv) script with the following changes:

  - On macOS, instead of deleting files, moves them to Trash;
  - Temp files created by `vimv` now have templated file names, making it possible to autodetect those.


## Credits

All the credits, of course, go to the [original authors](https://github.com/ivanmaeder/vimv/graphs/contributors).
